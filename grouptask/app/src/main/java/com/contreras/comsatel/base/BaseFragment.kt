package com.contreras.comsatel.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

/**
 * Created by Renso on 21/07/20.
 */
abstract class BaseFragment:Fragment() {

    abstract fun getIdLayout(): Int
    abstract fun initView(bundle: Bundle?)
    abstract fun setupView(bundle: Bundle?)
    abstract fun onRestoreView(savedInstanceState: Bundle)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getIdLayout(), container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(arguments)
        savedInstanceState?.let { onRestoreView(it) } ?: setupView(arguments)
    }

    fun showFullScreenLoader() {
        val activity = requireActivity()
        if (isViewExists() && activity is BaseActivity) {
            activity.showFullScreenLoader()
        }
    }

    fun dismissFullScreenLoader() {
        val activity = requireActivity()
        if (isViewExists() && activity is BaseActivity) {
            activity.dismissFullScreenLoader()
        }
    }

    fun isViewExists():Boolean = (isAdded && activity!=null)
}