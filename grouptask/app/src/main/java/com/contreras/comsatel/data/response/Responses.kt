package com.contreras.comsatel.data.response

import com.contreras.dog.data.response.ServerResponse
import com.google.gson.annotations.SerializedName

/**
 * Created by Renso on 30/07/20.
 */
class ErrorResponse(status: Int, message: String) : ServerResponse<Nothing>(status, message,null)

typealias GroupInServerResponse = ServerResponse<List<GroupData>>

typealias TaskInServerResponse = ServerResponse<List<TaskData>>

typealias GeneralServerResponse = ServerResponse<Any>


data class GroupData(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("status") val status: Int?,
    @SerializedName("total") val total: Int?)

data class TaskData(
    @SerializedName("id") val id: Int?,
    @SerializedName("group_id") val groupId: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("status") val status: Int?)

