package com.contreras.comsatel.data.source.group

import com.contreras.comsatel.data.remote.api.GroupInApi
import com.contreras.comsatel.data.remote.retrofit.NetworkHandler
import com.contreras.comsatel.data.request.GroupInRequest
import retrofit2.Retrofit

/**
 * Created by Renso on 09/08/20.
 */
class GroupRemoteDataSource(retrofit: Retrofit, private val networkHandler: NetworkHandler) {
    private val api by lazy { retrofit.create(GroupInApi::class.java) }
    suspend fun getListGroupByStatus(request: GroupInRequest) = networkHandler.handleServerResponse { api.getListGroupByStatus(request) }
    suspend fun createGroup(request: GroupInRequest) = networkHandler.handleServerResponse { api.createGroup(request) }
    suspend fun deleteGroup(request: GroupInRequest) = networkHandler.handleServerResponse { api.deleteGroup(request) }
    suspend fun editGroup(request: GroupInRequest)   = networkHandler.handleServerResponse { api.editGroup(request) }
}