package com.contreras.comsatel.data.source.group

import com.contreras.comsatel.model.Group
import com.contreras.comsatel.data.remote.retrofit.NetworkHandler
import com.contreras.comsatel.data.request.GroupInRequest
import com.contreras.comsatel.data.response.NullDataExpectedException
import com.contreras.comsatel.data.response.Result

interface GroupRepository {
    suspend fun getListGroupByStatus(status: Int?): Result<List<Group>>
    suspend fun createGroup(name: String?): Result<String>
    suspend fun deleteGroup(id: Int?): Result<String>
    suspend fun editGroup(id: Int?, name:String?): Result<String>

    class GroupRepositoryImpl() : GroupRepository {
        private lateinit var remoteSource: GroupRemoteDataSource

        constructor(remoteSource: GroupRemoteDataSource) : this() {
            this.remoteSource = remoteSource
        }

        override suspend fun getListGroupByStatus(status: Int?): Result<List<Group>> {
            val result = remoteSource.getListGroupByStatus(GroupInRequest(null, null, status))
            return when (result) {
                is Result.Success -> {
                    if (result.response?.data != null) {
                        val listGroup = mutableListOf<Group>()
                        for (groupData in result.response.data.orEmpty()) listGroup.add(Group.fromGroupData(groupData))
                        Result.Success(listGroup)
                    } else Result.Failure(NullDataExpectedException(NetworkHandler.DEFAULT_ERROR))
                }
                is Result.Failure -> result
            }
        }

        override suspend fun createGroup(name: String?): Result<String> {
            val result = remoteSource.createGroup(GroupInRequest(null, name, null))
            return when (result) {
                is Result.Success -> Result.Success(result.response?.message)
                is Result.Failure -> result
            }
        }

        override suspend fun deleteGroup(id: Int?): Result<String> {
            val result = remoteSource.deleteGroup(GroupInRequest(id, null, null))
            return when (result) {
                is Result.Success -> Result.Success(result.response?.message)
                is Result.Failure -> result
            }
        }

        override suspend fun editGroup(id: Int?, name: String?): Result<String> {
            val result = remoteSource.editGroup(GroupInRequest(id, name, null))
            return when (result) {
                is Result.Success -> Result.Success(result.response?.message)
                is Result.Failure -> result
            }
        }
    }
}