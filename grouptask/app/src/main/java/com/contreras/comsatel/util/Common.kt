package com.contreras.comsatel.util

import android.content.Context
import android.util.Patterns
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.contreras.comsatel.R
import com.contreras.comsatel.base.BaseActivity
import com.contreras.comsatel.base.BaseFragment
import com.contreras.comsatel.base.BaseViewModel
import com.google.android.material.snackbar.Snackbar

/**
 * Created by Renso on 29/07/20.
 */


fun Context.toast(message: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(this, message, duration).show()
}

fun BaseFragment.toast(message: String, duration: Int = Toast.LENGTH_LONG) {
    Toast.makeText(requireContext(), message, duration).show()
}

fun String.isValidEmail():Boolean= Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun BaseFragment.registerFullScreenLoadingObserver(viewModel: BaseViewModel) {
    viewModel.isViewLoading.observe(viewLifecycleOwner, Observer { loading ->
        if (loading) showFullScreenLoader() else dismissFullScreenLoader()
    })
}

fun BaseFragment.registerSnackBarErrorMessageObserver(viewModel: BaseViewModel) {
    viewModel.onErrorMessage.observe(viewLifecycleOwner, Observer { message ->
        showErrorMessage(message)
    })
}

fun Fragment.showErrorMessage(message: String?, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(requireView(), message ?: getString(R.string.default_error_message), duration).show()
}

fun BaseFragment.registerSnackBarSuccessMessageObserver(viewModel: BaseViewModel) {
    viewModel.onSuccessMessage.observe(viewLifecycleOwner, Observer { message ->
        showSuccessMessage(message)
    })
}

fun Fragment.showSuccessMessage(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(requireView(), message, duration).show()
}


fun BaseActivity.registerFullScreenLoadingObserver(viewModel: BaseViewModel) {
    viewModel.isViewLoading.observe(this, Observer { loading ->
        if (loading) showFullScreenLoader() else dismissFullScreenLoader()
    })
}

fun BaseActivity.registerSnackBarErrorMessageObserver(viewModel: BaseViewModel) {
    viewModel.onErrorMessage.observe(this, Observer { message ->
        showErrorMessage(message)
    })
}

fun BaseActivity.showErrorMessage(message: String?, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(findViewById(android.R.id.content), message ?: getString(R.string.default_error_message), duration).show()
}

fun BaseActivity.showSuccessMessage(message: String, duration: Int = Snackbar.LENGTH_LONG) {
    Snackbar.make(findViewById(android.R.id.content), message, duration).show()
}

