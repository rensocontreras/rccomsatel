package com.contreras.comsatel.util

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.contreras.comsatel.MainActivity
import com.contreras.comsatel.view.task.TaskActivity

/**
 * Created by Renso on 12/09/20.
 */
class Navigator() {
    companion object{
        fun navigateToMainActivity(context: Context?) {
            val intent = Intent(context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            context?.startActivity(intent)
        }

        fun navigateToTaskActivity(context: Context?, bundle: Bundle) {
            val intent = Intent(context, TaskActivity::class.java)
            intent.putExtras(bundle)
            context?.startActivity(intent)
        }
    }
}