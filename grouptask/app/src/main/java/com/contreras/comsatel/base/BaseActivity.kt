package com.contreras.comsatel.base

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.contreras.comsatel.R
import com.gu.toolargetool.TooLargeTool

/**
 * Created by Renso on 21/07/20.
 */
abstract class BaseActivity : AppCompatActivity(){

    private var loader: Dialog? = null

    abstract fun getIdLayout(): Int
    abstract fun initView(bundle: Bundle?)
    abstract fun setupView(bundle: Bundle?)
    abstract fun onRestoreView(savedInstanceState: Bundle)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getIdLayout())
        initView(intent.extras)
        savedInstanceState?.let { onRestoreView(it) } ?: setupView(intent.extras)
        TooLargeTool.startLogging(application)
    }

    fun showFullScreenLoader() {
        if (loader == null) {
            loader = Dialog(this)
        }
        if (loader?.isShowing == false) {
            loader?.setCancelable(false)
            loader?.window?.setBackgroundDrawableResource(android.R.color.transparent)
            loader?.setContentView(R.layout.dialog_full_screen_loading_indicator)
            loader?.show()
        }
    }

    fun dismissFullScreenLoader() {
        if (loader?.isShowing == true) {
            loader?.dismiss()
        }
    }

}