package com.contreras.comsatel.di

import com.contreras.comsatel.data.remote.retrofit.ApiClient
import com.contreras.comsatel.data.remote.retrofit.NetworkHandler
import com.contreras.comsatel.data.source.group.GroupRemoteDataSource
import com.contreras.comsatel.data.source.group.GroupRepository
import com.contreras.comsatel.data.source.task.TaskRemoteDataSource
import com.contreras.comsatel.data.source.task.TaskRepository
import com.contreras.comsatel.viewmodel.GroupViewModel
import com.contreras.comsatel.viewmodel.TaskViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Renso on 11/09/20.
 */
val appModule= module {
    single { ApiClient.create() }
    single { NetworkHandler() }

    single { GroupRemoteDataSource(get(), get()) }
    single { GroupRepository.GroupRepositoryImpl(get()) as GroupRepository }

    single { TaskRemoteDataSource(get(), get()) }
    single { TaskRepository.TaskRepositoryImpl(get()) as TaskRepository }

    viewModel {
        GroupViewModel(get())
    }

    viewModel {
        TaskViewModel(get(), get())
    }
}