package com.contreras.comsatel.data.remote.api

import com.contreras.comsatel.data.request.GroupInRequest
import com.contreras.comsatel.data.request.TaskInRequest
import com.contreras.comsatel.data.response.GeneralServerResponse
import com.contreras.comsatel.data.response.GroupInServerResponse
import com.contreras.comsatel.data.response.TaskInServerResponse
import retrofit2.Response
import retrofit2.http.*

interface GroupInApi {

    @POST("group/list_group.php")
    suspend fun getListGroupByStatus(@Body request: GroupInRequest): Response<GroupInServerResponse>

    @POST("group/register_group.php")
    suspend fun createGroup(@Body request: GroupInRequest): Response<GeneralServerResponse>

    @POST("group/delete_group.php")
    suspend fun deleteGroup(@Body request: GroupInRequest): Response<GeneralServerResponse>

    @POST("group/update_group.php")
    suspend fun editGroup(@Body request: GroupInRequest): Response<GeneralServerResponse>
}

interface TaskInApi {

    @POST("task/list_task.php")
    suspend fun getListTask(@Body request: TaskInRequest): Response<TaskInServerResponse>

    @POST("task/register_task.php")
    suspend fun createTask(@Body request: TaskInRequest): Response<GeneralServerResponse>

    @POST("task/delete_task.php")
    suspend fun deleteTask(@Body request: TaskInRequest): Response<GeneralServerResponse>

    @POST("task/update_task.php")
    suspend fun updateStatusTask(@Body request: TaskInRequest): Response<GeneralServerResponse>

    @POST("task/edit_task.php")
    suspend fun updateNameTask(@Body request: TaskInRequest): Response<GeneralServerResponse>
}