package com.contreras.comsatel.view.task

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.contreras.comsatel.R
import com.contreras.comsatel.adapter.RvTaskAdapter
import com.contreras.comsatel.base.BaseActivity
import com.contreras.comsatel.model.Group
import com.contreras.comsatel.model.Task
import com.contreras.comsatel.util.*
import com.contreras.comsatel.util.Constant.Companion.BUNDLE_GROUP
import com.contreras.comsatel.util.Constant.Companion.BUNDLE_IS_REFRESED
import com.contreras.comsatel.util.Constant.Companion.DELETE
import com.contreras.comsatel.viewmodel.TaskViewModel
import kotlinx.android.synthetic.main.activity_task.*
import kotlinx.android.synthetic.main.toolbar.*
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Created by Renso on 11/09/20.
 */
class TaskActivity:BaseActivity(), ICallback.ITask {

    private val viewModel: TaskViewModel by viewModel()
    private lateinit var rvTaskAdapter: RvTaskAdapter
    private var listTasks= mutableListOf<Task>()
    private var dialog: Dialog?=null
    private var group: Group?=null
    private var isRefeshed:Boolean = false

    override fun getIdLayout(): Int {
        return R.layout.activity_task
    }

    override fun initView(bundle: Bundle?) {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayShowTitleEnabled(true)
            this.title = getString(R.string.task_title)
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }

        toolbar.setNavigationIcon(R.drawable.ic_arrow_left);

        setupViewModel()
        ui()
    }

    override fun setupView(bundle: Bundle?) {
        if(bundle!=null) {
            group = bundle.getParcelable(BUNDLE_GROUP) as? Group
            group?.let {
                viewModel.setGroupId(it.id)
                viewModel.getAll(it.id)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBoolean(BUNDLE_IS_REFRESED, isRefeshed)
        outState.putParcelable(BUNDLE_GROUP, group)
    }

    override fun onRestoreView(savedInstanceState: Bundle) {
        isRefeshed = savedInstanceState.getBoolean(BUNDLE_IS_REFRESED)
        group = savedInstanceState.getParcelable(BUNDLE_GROUP)
    }

    private fun ui() {
        rvTasks.layoutManager = LinearLayoutManager(this)
        rvTaskAdapter = RvTaskAdapter(R.layout.item_task, mutableListOf(), this)
        rvTasks.adapter = rvTaskAdapter

         fabCreate.setOnClickListener {
             dialog = DialogUtil.showItemDialog(this, getString(R.string.task_create),
                 getString(R.string.task_hint_name)){
                 it?.let { texto -> group?.id.let{ id->
                     viewModel.createTask(id,texto)
                    }
                 }
             }
        }
    }

    private fun setupViewModel() {
        registerFullScreenLoadingObserver(viewModel)
        registerSnackBarErrorMessageObserver(viewModel)
        viewModel.onSuccessMessage.observe(this, readSuccessMessageObserver)
        viewModel.listTasks.observe(this, readListTasksObserver)
    }

    private val readSuccessMessageObserver = Observer<String> {
        it?.let { message ->
            isRefeshed = true
            showSuccessMessage(message)
        }
    }

    private val readListTasksObserver = Observer<List<Task>> {
        if(it!=null){
            listTasks = it.toMutableList()
            rvTaskAdapter.update(listTasks)
        }
    }

    override fun editTask(task: Task?) {
        dialog = DialogUtil.operateTaskDialog(this, getString(R.string.task_edit),
            getString(R.string.task_hint_name), Constant.EDIT, task){
            viewModel.updateNameTask(it?.id, it?.name)
        }
    }

    override fun deleteTask(task: Task?) {
        dialog = DialogUtil.operateTaskDialog(this, getString(R.string.task_edit),
            getString(R.string.task_hint_name), DELETE, task){
            viewModel.deleteTask(it?.id)
        }
    }

    override fun updateTask(task: Task?) {
        task?.let{ it->
            viewModel.updateStatusTask(it.id, it.status)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        val intent = Intent()
        if(isRefeshed){
            setResult(RESULT_OK, intent)
        } else setResult(RESULT_CANCELED, intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog?.dismiss()
    }
}