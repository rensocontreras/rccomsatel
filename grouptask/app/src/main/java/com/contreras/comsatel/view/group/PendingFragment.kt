package com.contreras.comsatel.view.group

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.contreras.comsatel.MainActivity
import com.contreras.comsatel.R
import com.contreras.comsatel.adapter.RvGroupsAdapter
import com.contreras.comsatel.base.BaseFragment
import com.contreras.comsatel.model.Group
import com.contreras.comsatel.util.*
import com.contreras.comsatel.util.Constant.Companion.DELETE
import com.contreras.comsatel.util.Constant.Companion.EDIT
import com.contreras.comsatel.util.Constant.Companion.REQUEST_CODE
import com.contreras.comsatel.view.task.TaskActivity
import com.contreras.comsatel.viewmodel.GroupViewModel
import kotlinx.android.synthetic.main.fragment_complete.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

/**
 * Created by Renso on 10/09/20.
 */
class PendingFragment:BaseFragment(), ICallback.IGroup {

    private val viewModel: GroupViewModel by sharedViewModel()
    private lateinit var rvGroupsAdapter: RvGroupsAdapter
    private var listGroupsPending= mutableListOf<Group>()
    private var dialog: Dialog?=null

    override fun getIdLayout(): Int {
        return R.layout.fragment_pending
    }

    override fun initView(bundle: Bundle?) {
        ui()
    }

    override fun setupView(bundle: Bundle?) {
        setupViewModel()
    }

    override fun onRestoreView(savedInstanceState: Bundle) {
        setupViewModel()
    }

    private fun setupViewModel() {
        registerFullScreenLoadingObserver(viewModel)
        registerSnackBarErrorMessageObserver(viewModel)
        viewModel.onSuccessMessage.observe(viewLifecycleOwner, readSuccessMessageObserver)
        viewModel.listGroupsPending.observe(viewLifecycleOwner, readListGroupsPending)
    }

    private fun ui() {
        rvGroups.layoutManager = LinearLayoutManager(activity)
        //emptyList()   ,  arrayListOf()
        rvGroupsAdapter = RvGroupsAdapter(R.layout.item_group, mutableListOf(), this){
            val bundle = Bundle()
            bundle.putParcelable(Constant.BUNDLE_GROUP, it)
            val intent = Intent(requireActivity(), TaskActivity::class.java)
            intent.putExtras(bundle)
            startActivityForResult(intent, REQUEST_CODE)
        }
        rvGroups.adapter = rvGroupsAdapter
    }

    private val readListGroupsPending = Observer<List<Group>> {
        if(it!=null){
            listGroupsPending = it.toMutableList()
            rvGroupsAdapter.update(listGroupsPending)
            //viewModel.clearListGroupPending()
        }
    }

    private val readSuccessMessageObserver = Observer<String> {
        it?.let { message -> showSuccessMessage(message) }
    }

    override fun editGroup(group: Group?) {
        dialog = DialogUtil.operateGroupDialog(requireContext(), getString(R.string.main_edit_group),
            getString(R.string.main_hint_group), EDIT, group){
            viewModel.editGroup(it?.id, it?.name)
        }
    }

    override fun deleteGroup(group: Group?) {
        dialog = DialogUtil.operateGroupDialog(requireContext(), getString(R.string.main_delete_group),
            getString(R.string.main_hint_group), DELETE, group){
            viewModel.deleteGroup(it?.id)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                viewModel.getAllList()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog?.dismiss()
    }
}