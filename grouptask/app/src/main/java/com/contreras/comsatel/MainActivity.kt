package com.contreras.comsatel

import android.app.Dialog
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.ui.setupWithNavController
import com.contreras.comsatel.base.BaseActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.contreras.comsatel.util.DialogUtil
import com.contreras.comsatel.util.registerFullScreenLoadingObserver
import com.contreras.comsatel.util.registerSnackBarErrorMessageObserver
import com.contreras.comsatel.util.showSuccessMessage
import com.contreras.comsatel.viewmodel.GroupViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity() {

    private val viewModel: GroupViewModel by viewModel()
    lateinit var navView: BottomNavigationView
    private var dialog: Dialog?=null

    override fun getIdLayout(): Int {
        return R.layout.activity_main;
    }

    override fun initView(bundle: Bundle?) {
        navView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)

        val appBarConfiguration = AppBarConfiguration(setOf(
            R.id.nav_pending, R.id.nav_complete))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        supportActionBar?.hide()

        setupViewModel()
        ui()
    }

    private fun ui() {
        fabCreate.setOnClickListener {
            dialog = DialogUtil.showItemDialog(this, getString(R.string.main_create_group),
                getString(R.string.main_hint_group)){
                it?.let { texto -> viewModel.createGroup(texto) }

            }
        }
    }

    override fun setupView(bundle: Bundle?) {
        viewModel.getAllList()
    }

    override fun onRestoreView(savedInstanceState: Bundle) {
    }

    private fun setupViewModel() {
        registerFullScreenLoadingObserver(viewModel)
        registerSnackBarErrorMessageObserver(viewModel)
        viewModel.onSuccessMessage.observe(this, readSuccessMessageObserver)
    }

    private val readSuccessMessageObserver = Observer<String> {
        it?.let { message -> showSuccessMessage(message) }
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog?.dismiss()
    }
}