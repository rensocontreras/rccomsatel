package com.contreras.comsatel.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Created by Renso on 03/08/20.
 */
abstract class BaseViewModel : ViewModel()  {

    private val _isViewLoading= MutableLiveData<Boolean>()
    val isViewLoading: LiveData<Boolean> = _isViewLoading

    private val _onSuccessMessage= MutableLiveData<String>()
    val onSuccessMessage: LiveData<String> = _onSuccessMessage

    private val _onErrorMessage= MutableLiveData<String>()
    val onErrorMessage: LiveData<String> = _onErrorMessage

    fun startLoading(){
        _isViewLoading.postValue(true)
    }

    fun stopLoading(){
        _isViewLoading.postValue(false)
    }

    fun showSuccessMessage(message: String?){
        _onSuccessMessage.postValue(message)
    }

    fun showErrorMessage(exception: Exception?){
        _onErrorMessage.postValue(exception?.message)
    }
}