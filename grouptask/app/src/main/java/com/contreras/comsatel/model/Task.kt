package com.contreras.comsatel.model

import android.os.Parcelable
import com.contreras.comsatel.data.response.TaskData
import kotlinx.android.parcel.Parcelize

/**
 * Created by Renso on 11/09/20.
 */
@Parcelize
data class Task(
    var id: Int? = null,
    var groupId: Int? = null,
    var name: String? = null,
    var status:Int? = null):
    Parcelable {

    companion object {
        fun fromTaskData(data: TaskData): Task {
            return Task().apply {
                this.id      = data.id
                this.groupId = data.groupId
                this.name    = data.name
                this.status  = data.status
            }
        }
    }

    override fun toString(): String {
        return name!!
    }
}