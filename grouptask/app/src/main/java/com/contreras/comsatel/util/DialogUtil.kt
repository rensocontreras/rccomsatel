package com.contreras.comsatel.util

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.contreras.comsatel.R
import com.contreras.comsatel.model.Group
import com.contreras.comsatel.model.Task
import com.contreras.comsatel.util.Constant.Companion.EDIT

/**
 * Created by Renso on 11/09/20.
 */
class DialogUtil {

    companion object {

        fun setupDialog(ctx: Context?, res: Int): Dialog {
            val dialog = Dialog(ctx!!)
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(res)
            dialog.window?.let {
                val lp = it.attributes
                lp.dimAmount = 0.8f
                it.attributes = lp
            }
            return dialog
        }

        fun showItemDialog(context: Context?, title: String?, txtHint: String?,
                           itemCallback: (name: String?) -> Unit?): Dialog? {
            if (context != null && !(context as Activity).isFinishing) {
                val dialog: Dialog = setupDialog(context, R.layout.custom_dialog_group)
                val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)
                tvTitle.text = title

                val edtInput = dialog.findViewById<EditText>(R.id.edtInput)
                edtInput.hint = txtHint

                val tvCancel = dialog.findViewById<TextView>(R.id.tvCancel)
                tvCancel.setOnClickListener { view: View? -> dialog.dismiss() }

                val tvAction = dialog.findViewById<TextView>(R.id.tvAction)
                tvAction.setOnClickListener { view: View? ->
                    run {
                        val name = edtInput.text?.trim().toString()
                        if(name.isNullOrEmpty()) Toast.makeText(context, context.getString(R.string.message_empty), Toast.LENGTH_SHORT).show()
                        else{
                            dialog.dismiss()
                            itemCallback(name)
                        }
                    }
                }

                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()
                return dialog
            }else return null
        }

        fun operateGroupDialog(context: Context?, title: String?, txtHint: String?, type:Int?,
                               group: Group?, itemCallback: (group: Group?) -> Unit?):Dialog? {
            if (context != null && !(context as Activity).isFinishing) {
                val dialog: Dialog = setupDialog(context, R.layout.custom_dialog_group)
                val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)
                tvTitle.text = title

                val tvCancel = dialog.findViewById<TextView>(R.id.tvCancel)
                tvCancel.setOnClickListener { view: View? -> dialog.dismiss() }

                val tvInput = dialog.findViewById<TextView>(R.id.tvInput)
                val edtInput = dialog.findViewById<EditText>(R.id.edtInput)
                val tvAction= dialog.findViewById<TextView>(R.id.tvAction)

                when(type){
                    EDIT ->{
                        tvInput.visibility = View.GONE
                        edtInput.visibility = View.VISIBLE
                        edtInput.hint = txtHint
                        edtInput.setText(group?.name)
                        tvAction.text = context.getString(R.string.main_save)
                    }
                    else ->{
                        edtInput.visibility = View.GONE
                        tvInput.visibility = View.VISIBLE
                        tvInput.text = group?.name
                        tvAction.text = context.getString(R.string.main_delete)
                    }
                }

                tvAction.setOnClickListener { view: View? ->
                    run {
                        dialog.dismiss()
                        when(type){
                            EDIT->{
                                val groupName = edtInput.text?.trim().toString()
                                if(groupName.isNullOrEmpty()) Toast.makeText(context, context.getString(R.string.message_empty), Toast.LENGTH_SHORT).show()
                                else{
                                    group?.run { this.name = groupName }
                                    itemCallback(group)
                                }
                            }
                            else -> itemCallback(group)
                        }
                    }
                }

                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()
                return dialog
            }else return null
        }

        fun operateTaskDialog(context: Context?, title: String?, txtHint: String?, type:Int?,
                              task: Task?, itemCallback: (task: Task?) -> Unit?):Dialog? {
            if (context != null && !(context as Activity).isFinishing) {
                val dialog: Dialog = setupDialog(context, R.layout.custom_dialog_group)
                val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)
                tvTitle.text = title

                val tvCancel = dialog.findViewById<TextView>(R.id.tvCancel)
                tvCancel.setOnClickListener { view: View? -> dialog.dismiss() }

                val tvInput = dialog.findViewById<TextView>(R.id.tvInput)
                val edtInput = dialog.findViewById<EditText>(R.id.edtInput)
                val tvAction= dialog.findViewById<TextView>(R.id.tvAction)

                when(type){
                    EDIT ->{
                        tvInput.visibility = View.GONE
                        edtInput.visibility = View.VISIBLE
                        edtInput.hint = txtHint
                        edtInput.setText(task?.name)
                        tvAction.text = context.getString(R.string.main_save)
                    }
                    else ->{
                        edtInput.visibility = View.GONE
                        tvInput.visibility = View.VISIBLE
                        tvInput.text = task?.name
                        tvAction.text = context.getString(R.string.main_delete)
                    }
                }

                tvAction.setOnClickListener { view: View? ->
                    run {
                        when(type){
                            EDIT->{
                                val taskName = edtInput.text?.trim().toString()
                                if(taskName.isNullOrEmpty()) Toast.makeText(context, context.getString(R.string.message_empty), Toast.LENGTH_SHORT).show()
                                else{
                                    task?.run { this.name = taskName  }
                                    itemCallback(task)
                                }
                            }
                            else ->{
                                dialog.dismiss()
                                itemCallback(task)
                            }
                        }
                    }
                }

                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                dialog.show()
                return dialog
            }else return null
        }
    }
}