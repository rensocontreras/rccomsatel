package com.contreras.comsatel.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.contreras.comsatel.base.BaseViewModel
import com.contreras.comsatel.data.remote.retrofit.NetworkHandler
import com.contreras.comsatel.data.response.NullDataExpectedException
import com.contreras.comsatel.data.source.group.GroupRepository
import com.contreras.comsatel.model.Group
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.contreras.comsatel.data.response.Result

/**
 * Created by Renso on 11/09/20.
 */
class GroupViewModel(private val groupRepository: GroupRepository):BaseViewModel() {

    private val _listGroupsPending = MutableLiveData<List<Group>>()
    val listGroupsPending: LiveData<List<Group>> = _listGroupsPending

    private val _listGroupsComplete = MutableLiveData<List<Group>>()
    val listGroupsComplete: LiveData<List<Group>> = _listGroupsComplete

    fun getAllList(){
        startLoading()
        viewModelScope.launch {
            val defListGroupPendingResult =  async (Dispatchers.IO){
                groupRepository.getListGroupByStatus(0)
            }
            val defListGroupCompleteResult =  async(Dispatchers.IO){
                groupRepository.getListGroupByStatus(1)
            }
            useValues(defListGroupPendingResult.await(), defListGroupCompleteResult.await())
        }
    }

    private fun useValues(listGroupPendingResult: Result<List<Group>>, listGroupCompleteResult: Result<List<Group>>) {

        if(listGroupPendingResult is Result.Success && listGroupCompleteResult is Result.Success){
            val listGroupPending   = listGroupPendingResult.response
            val listComplete       = listGroupCompleteResult.response
            _listGroupsPending.value  = listGroupPending
            _listGroupsComplete.value = listComplete
            showSuccessMessage(null)
            stopLoading()
        }else{
            stopLoading()
            if(listGroupPendingResult is Result.Failure && listGroupCompleteResult is Result.Failure){
                showErrorMessage(NullDataExpectedException(NetworkHandler.DEFAULT_ERROR))
            }else{
                stopLoading()
                when (listGroupPendingResult) {
                    is Result.Success -> _listGroupsPending.value = listGroupPendingResult.response
                    is Result.Failure -> showErrorMessage(listGroupPendingResult.exception)
                }
                when (listGroupCompleteResult) {
                    is Result.Success -> _listGroupsComplete.value = listGroupCompleteResult.response
                    is Result.Failure -> showErrorMessage(listGroupCompleteResult.exception)
                }
            }
        }
    }

    fun createGroup(name:String){
        startLoading()
        viewModelScope.launch {
            val result =  withContext (Dispatchers.IO){
                groupRepository.createGroup(name)
            }

            stopLoading()
            when (result) {
                is Result.Success ->{
                    showSuccessMessage(result.response)
                    getAllList()
                }
                is Result.Failure -> showErrorMessage(result.exception)
            }
        }
    }

    fun deleteGroup(id: Int?) {
        startLoading()
        viewModelScope.launch {
            val result =  withContext (Dispatchers.IO){
                groupRepository.deleteGroup(id)
            }

            stopLoading()
            getAllList()
            when (result) {
                is Result.Success -> showSuccessMessage(result.response)
                is Result.Failure -> showErrorMessage(result.exception)
            }
        }
    }

    fun editGroup(id: Int?, name: String?) {
        startLoading()
        viewModelScope.launch {
            val result =  withContext (Dispatchers.IO){
                groupRepository.editGroup(id, name)
            }

            stopLoading()
            getAllList()
            when (result) {
                is Result.Success -> showSuccessMessage(result.response)
                is Result.Failure -> showErrorMessage(result.exception)
            }
        }
    }
}

/*
    fun clearListGroupPending() {
        _listGroupsPending.value  = null
    }

    fun clearListGroupComplete() {
        _listGroupsComplete.value  = null
    }
 */