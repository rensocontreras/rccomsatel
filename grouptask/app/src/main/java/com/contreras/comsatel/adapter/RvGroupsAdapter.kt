package com.contreras.comsatel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.contreras.comsatel.model.Group
import com.contreras.comsatel.util.ICallback
import kotlinx.android.synthetic.main.item_group.view.*

/**
 * Created by Renso on 19/08/20.
 */
class RvGroupsAdapter(private val idItemLayout:Int,
                      private var listGroups:MutableList<Group>,
                      private val callback:ICallback.IGroup,
                      private val itemCallback: (group: Group) -> Unit?)
    :RecyclerView.Adapter<RvGroupsAdapter.GroupViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroupViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(idItemLayout, parent,false)
        return GroupViewHolder(view)
    }

    override fun onBindViewHolder(holder: GroupViewHolder, position: Int) {
        holder.bind(listGroups[position])
        holder.view.clItemGroup.setOnClickListener {
            itemCallback(listGroups[position])
        }
    }

    override fun getItemCount(): Int {
        return listGroups.size
    }

    fun update(data:MutableList<Group>){
        listGroups= data
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        listGroups.removeAt(position)
        notifyItemRemoved(position)
    }

    private fun deleteGroup(layoutPosition: Int) {
        callback.deleteGroup(listGroups[layoutPosition])
    }

    private fun editGroup(layoutPosition: Int) {
        callback.editGroup(listGroups[layoutPosition])
    }

    inner class GroupViewHolder(val view: View): RecyclerView.ViewHolder(view){
        fun bind(group:Group){
            view.tvGroupName.text  = group.name
            view.tvTotal.text = group.total.toString()

            view.ivEdit.setOnClickListener {
                editGroup(layoutPosition)
            }

            view.ivDelete.setOnClickListener {
                deleteGroup(layoutPosition)
            }
        }
    }
}