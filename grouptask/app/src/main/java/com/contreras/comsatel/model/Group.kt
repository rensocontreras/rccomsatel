package com.contreras.comsatel.model

import android.os.Parcelable
import com.contreras.comsatel.data.response.GroupData
import kotlinx.android.parcel.Parcelize

/**
 * Created by Renso on 11/09/20.
 */
@Parcelize
data class Group(
    var id: Int? = null,
    var name: String? = null,
    var status:Int? = null,
    var total:Int? = null):
    Parcelable {

    companion object {
        fun fromGroupData(data: GroupData): Group {
            return Group().apply {
                this.id     = data.id
                this.name   = data.name
                this.status = data.status
                this.total  = data.total
            }
        }
    }

    override fun toString(): String {
        return name!!
    }
}