package com.contreras.comsatel.data.source.task

import com.contreras.comsatel.data.remote.retrofit.NetworkHandler
import com.contreras.comsatel.data.request.TaskInRequest
import com.contreras.comsatel.data.response.NullDataExpectedException
import com.contreras.comsatel.data.response.Result
import com.contreras.comsatel.model.Task

interface TaskRepository {
    suspend fun getListTask(idGroup: Int?): Result<List<Task>>
    suspend fun createTask(idGroup:Int?, name: String?): Result<String>
    suspend fun deleteTask(idTask: Int?): Result<String>
    suspend fun updateStatusTask(idTask: Int?, status:Int?): Result<String>
    suspend fun updateNameTask(idTask: Int?, name:String?): Result<String>

    class TaskRepositoryImpl() : TaskRepository {
        private lateinit var remoteSource: TaskRemoteDataSource

        constructor(remoteSource: TaskRemoteDataSource) : this() {
            this.remoteSource = remoteSource
        }

        override suspend fun getListTask(idGroup: Int?): Result<List<Task>> {
            val result = remoteSource.getListTask(TaskInRequest(null, idGroup, null, null))
            return when (result) {
                is Result.Success -> {
                    if (result.response?.data != null) {
                        val listTask = mutableListOf<Task>()
                        for (taskData in result.response.data.orEmpty()) listTask.add(Task.fromTaskData(taskData))
                        Result.Success(listTask)
                    } else Result.Failure(NullDataExpectedException(NetworkHandler.DEFAULT_ERROR))
                }
                is Result.Failure -> result
            }
        }

        override suspend fun createTask(idGroup:Int?, name: String?): Result<String> {
            val result = remoteSource.createTask(TaskInRequest(null, idGroup, name, null))
            return when (result) {
                is Result.Success -> Result.Success(result.response?.message)
                is Result.Failure -> result
            }
        }

        override suspend fun deleteTask(idTask: Int?): Result<String> {
            val result = remoteSource.deleteTask(TaskInRequest(idTask, null, null, null))
            return when (result) {
                is Result.Success -> Result.Success(result.response?.message)
                is Result.Failure -> result
            }
        }

        override suspend fun updateStatusTask(idTask: Int?, status:Int?): Result<String> {
            val result = remoteSource.updateStatusTask(TaskInRequest(idTask, null, null, status))
            return when (result) {
                is Result.Success -> Result.Success(result.response?.message)
                is Result.Failure -> result
            }
        }

        override suspend fun updateNameTask(idTask: Int?, name: String?): Result<String> {
            val result = remoteSource.updateNameTask(TaskInRequest(idTask, null, name, null))
            return when (result) {
                is Result.Success -> Result.Success(result.response?.message)
                is Result.Failure -> result
            }
        }
    }
}