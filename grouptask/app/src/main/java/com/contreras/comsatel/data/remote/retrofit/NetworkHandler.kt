package com.contreras.comsatel.data.remote.retrofit

import com.contreras.comsatel.data.response.ErrorResponse
import com.contreras.comsatel.data.response.NetworkException
import com.contreras.comsatel.data.response.ServerException
import com.contreras.comsatel.data.response.UnauthorizedException
import com.contreras.comsatel.data.response.*
import com.contreras.dog.data.response.ServerResponse
import com.google.gson.Gson
import retrofit2.Response
import java.lang.Exception
import okio.IOException

/**
 * Created by Renso on 30/07/20.
 */
class NetworkHandler {

    companion object {
        const val NETWORK_CONNECTION_ERROR = "Por favor verifique su conexión a internet e inténtelo nuevamente."
        const val DEFAULT_ERROR            = "Ha ocurrido un problema con el servidor, por favor inténtelo más tarde."
        const val UNAUTHORIZED_ERROR       = "Su sesión ha expirado. Por favor vuelva a iniciar sesión."
    }

    val defaultErrorResult: Result.Failure
        get() = Result.Failure(ServerException(DEFAULT_ERROR))

    inline fun <T> handleServerResponse(task: () -> Response<ServerResponse<T>>): Result<ServerResponse<T>> {
        try {
            val response = task()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) {
                    if (body.isProcessedSuccessfully) {
                        return Result.Success(body)
                    }
                    if (body.status == 401)
                        return Result.Failure(
                            UnauthorizedException(
                            if (body.message.isNotBlank()) body.message else UNAUTHORIZED_ERROR
                        )
                    )
                    if (body.message.isNotBlank()) {
                        return Result.Failure(ServerException(body.message))
                    }
                }
            } else {
                val errorResponseAsString= response.errorBody()?.string()
                val errorResponse = Gson().fromJson(errorResponseAsString, ErrorResponse::class.java)

                if (errorResponse != null) {
                    if (errorResponse.status == 401) {
                        return Result.Failure(
                            UnauthorizedException(
                                if (errorResponse.message.isNotBlank()) errorResponse.message else UNAUTHORIZED_ERROR
                            )
                        )
                    }
                    if (errorResponse.message.isNotBlank()) {
                        return Result.Failure(ServerException(errorResponse.message))
                    }
                } else {
                    if (response.code() == 401) {
                        return Result.Failure(UnauthorizedException(UNAUTHORIZED_ERROR))
                    }
                }
            }

            return defaultErrorResult

        } catch (e: Exception) {
            return if (e is IOException) {
                Result.Failure(NetworkException(NETWORK_CONNECTION_ERROR))
            }else defaultErrorResult
        }
    }
}