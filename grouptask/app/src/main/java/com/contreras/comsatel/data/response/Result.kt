package com.contreras.comsatel.data.response

/**
 * Created by Renso on 30/07/20.
 */

sealed class Result<out T> {
    data class Success<T>(val response: T?) : Result<T>()
    data class Failure(val exception: Exception) : Result<Nothing>()
}

class NullDataExpectedException(message: String?): Exception(message)
class NetworkException(message: String?): Exception(message)
class UnauthorizedException(message: String?): Exception(message)
class ServerException(message: String?): Exception(message)