package com.contreras.comsatel.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.contreras.comsatel.base.BaseViewModel
import com.contreras.comsatel.data.response.Result
import com.contreras.comsatel.data.source.group.GroupRepository
import com.contreras.comsatel.data.source.task.TaskRepository
import com.contreras.comsatel.model.Group
import com.contreras.comsatel.model.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Created by Renso on 11/09/20.
 */
class TaskViewModel(private val groupRepository: GroupRepository,
                    private val taskRepository: TaskRepository): BaseViewModel()  {

    private val _listGroups = MutableLiveData<List<Group>>()
    val listGroups: LiveData<List<Group>> = _listGroups

    private val _listTasks = MutableLiveData<List<Task>>()
    val listTasks: LiveData<List<Task>> = _listTasks

    private val _groupId = MutableLiveData<Int>()

    fun setGroupId(id: Int?) {
        _groupId.value = id
    }

    fun getAll(idGroup: Int?) {
        startLoading()
        viewModelScope.launch {
            val listTasksResult =  withContext (Dispatchers.IO){
                taskRepository.getListTask(idGroup)
            }
            stopLoading()
            when (listTasksResult) {
                is Result.Success -> _listTasks.value = listTasksResult.response
                is Result.Failure -> showErrorMessage(listTasksResult.exception)
            }
        }
    }

    fun createTask(idGroup:Int?, name: String?){
        startLoading()
        viewModelScope.launch {
            val createResult =  withContext (Dispatchers.IO){
                taskRepository.createTask(idGroup, name)
            }
            stopLoading()
            when (createResult) {
                is Result.Success -> {
                    showSuccessMessage(createResult.response)
                    getAll(_groupId.value)
                }
                is Result.Failure -> showErrorMessage(createResult.exception)
            }
        }
    }

    fun updateStatusTask(idTask: Int?, status:Int?){
        startLoading()
        viewModelScope.launch {
            val updateStatusResult =  withContext (Dispatchers.IO){
                taskRepository.updateStatusTask(idTask, status)
            }
            stopLoading()
            when (updateStatusResult) {
                is Result.Success -> {
                    showSuccessMessage(updateStatusResult.response)
                    //getAll(_groupId.value)
                }
                is Result.Failure -> showErrorMessage(updateStatusResult.exception)
            }
        }
    }

    fun updateNameTask(idTask: Int?, name:String?){
        startLoading()
        viewModelScope.launch {
            val updateNameResult =  withContext (Dispatchers.IO){
                taskRepository.updateNameTask(idTask, name)
            }
            stopLoading()
            when (updateNameResult) {
                is Result.Success -> {
                    showSuccessMessage(updateNameResult.response)
                    getAll(_groupId.value)
                }
                is Result.Failure -> showErrorMessage(updateNameResult.exception)
            }
        }
    }

    fun deleteTask(idTask: Int?){
        startLoading()
        viewModelScope.launch {
            val deleteResult =  withContext (Dispatchers.IO){
                taskRepository.deleteTask(idTask)
            }
            stopLoading()
            when (deleteResult) {
                is Result.Success -> {
                    showSuccessMessage(deleteResult.response)
                    getAll(_groupId.value)
                }
                is Result.Failure -> showErrorMessage(deleteResult.exception)
            }
        }
    }
}