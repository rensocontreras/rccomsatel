package com.contreras.comsatel.base

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment

/**
 * Created by Renso on 03/09/20.
 */
abstract class BaseDialogFragment: DialogFragment() {

    private var mContext: Context? = null
    abstract fun getIdLayout(): Int
    abstract fun setupReceiver(bundle: Bundle?)
    abstract fun setupView(bundle: Bundle?)
    abstract fun onRestoreView(savedInstanceState: Bundle?)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        super.onCreateDialog(savedInstanceState)
        setupReceiver(arguments)
        val builder = AlertDialog.Builder(activity)
        val view: View = requireActivity().layoutInflater.inflate(getIdLayout(), null)
        savedInstanceState?.let { onRestoreView(it) } ?: setupView(arguments)
        builder.setView(view)
        return builder.create()
    }

    open fun showLoading() {
        (mContext as BaseActivity).showFullScreenLoader()
    }

    open fun dismissLoading() {
        (mContext as BaseActivity).dismissFullScreenLoader()
    }

    open fun isViewExists(): Boolean? {
        return activity != null && isAdded
    }
}