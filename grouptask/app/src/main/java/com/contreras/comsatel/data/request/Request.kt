package com.contreras.comsatel.data.request

import com.google.gson.annotations.SerializedName

/**
 * Created by Renso on 30/07/20.
 */
data class GroupInRequest(
    @SerializedName("txt_group_id") val id: Int?,
    @SerializedName("txt_name") val name: String?,
    @SerializedName("txt_status") val status: Int?)


data class TaskInRequest(
    @SerializedName("txt_task_id") val idTask: Int?,
    @SerializedName("txt_group_id") val idGroup: Int?,
    @SerializedName("txt_name") val name: String?,
    @SerializedName("txt_status") val status: Int?)

