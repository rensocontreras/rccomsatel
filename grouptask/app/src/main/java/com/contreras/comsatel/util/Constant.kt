package com.contreras.comsatel.util

/**
 * Created by Renso on 13/08/20.
 */
class Constant {
    companion object {
        const val BUNDLE_GROUP       = "BUNDLE_GROUP"
        const val BUNDLE_IS_REFRESED = "BUNDLE_IS_REFRESED"


        const val EDIT   = 0
        const val DELETE = 1

        const val REQUEST_CODE = 100
    }
}