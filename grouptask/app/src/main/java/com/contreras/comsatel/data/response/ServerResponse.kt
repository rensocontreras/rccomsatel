package com.contreras.dog.data.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Renso on 30/07/20.
 */
open class ServerResponse<T>(@SerializedName("status") val status: Int,
                             @SerializedName("message") val message: String,
                             @SerializedName("data") val data: T?) {
    val isProcessedSuccessfully
        get() = (status in 200..299)
}