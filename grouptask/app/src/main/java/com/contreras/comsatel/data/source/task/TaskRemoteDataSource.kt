package com.contreras.comsatel.data.source.task

import com.contreras.comsatel.data.remote.api.TaskInApi
import com.contreras.comsatel.data.remote.retrofit.NetworkHandler
import com.contreras.comsatel.data.request.TaskInRequest
import retrofit2.Retrofit

/**
 * Created by Renso on 11/09/20.
 */
class TaskRemoteDataSource(retrofit: Retrofit, private val networkHandler: NetworkHandler) {
    private val api by lazy { retrofit.create(TaskInApi::class.java) }

    suspend fun getListTask(request: TaskInRequest) = networkHandler.handleServerResponse { api.getListTask(request) }
    suspend fun createTask(request: TaskInRequest) = networkHandler.handleServerResponse { api.createTask(request) }
    suspend fun deleteTask(request: TaskInRequest) = networkHandler.handleServerResponse { api.deleteTask(request) }
    suspend fun updateStatusTask(request: TaskInRequest) = networkHandler.handleServerResponse { api.updateStatusTask(request) }
    suspend fun updateNameTask(request: TaskInRequest) = networkHandler.handleServerResponse { api.updateNameTask(request) }
}