package com.contreras.comsatel.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.contreras.comsatel.model.Task
import com.contreras.comsatel.util.ICallback
import kotlinx.android.synthetic.main.item_group.view.ivDelete
import kotlinx.android.synthetic.main.item_group.view.ivEdit
import kotlinx.android.synthetic.main.item_task.view.*

/**
 * Created by Renso on 19/08/20.
 */
class RvTaskAdapter(private val idItemLayout:Int,
                    private var listTasks:MutableList<Task>,
                    private val callback:ICallback.ITask)
    :RecyclerView.Adapter<RvTaskAdapter.TaskViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(idItemLayout, parent,false)
        return TaskViewHolder(view)
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(listTasks[position])
    }

    override fun getItemCount(): Int {
        return listTasks.size
    }

    fun update(data:MutableList<Task>){
        listTasks = data
        notifyDataSetChanged()
    }

    private fun deleteTask(layoutPosition: Int) {
        callback.deleteTask(listTasks[layoutPosition])
    }

    private fun editTask(layoutPosition: Int) {
        callback.editTask(listTasks[layoutPosition])
    }

    private fun updateCheckBox(checked: Boolean, layoutPosition: Int) {
        if(checked) listTasks[layoutPosition].status = 1
        else listTasks[layoutPosition].status = 0
        callback.updateTask(listTasks[layoutPosition])
    }

    inner class TaskViewHolder(val view: View): RecyclerView.ViewHolder(view){
        fun bind(task:Task){
            view.tvTaskName.text  = task.name.toString()

            view.ivEdit.setOnClickListener {
                editTask(layoutPosition)
            }

            view.ivDelete.setOnClickListener {
                deleteTask(layoutPosition)
            }

            view.chkStatus.isChecked = task.status != 0

            view.chkStatus.setOnCheckedChangeListener { compoundButton, isChecked ->
                updateCheckBox(compoundButton.isChecked, layoutPosition)
            }
        }
    }
}