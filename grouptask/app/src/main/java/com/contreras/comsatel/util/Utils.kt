package com.contreras.comsatel.util

import java.text.DecimalFormat


/**
 * Created by Renso on 02/09/20.
 */
class Utils {
    companion object{

        fun formatFloat(number: Float?):String{
            number?.let {
                val df = DecimalFormat("0.00")
                df.maximumFractionDigits = 2
                return df.format(it)
            }?: run{ return "0.00"}
        }

    }
}