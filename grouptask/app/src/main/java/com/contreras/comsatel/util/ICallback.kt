package com.contreras.comsatel.util

import com.contreras.comsatel.model.Group
import com.contreras.comsatel.model.Task

/**
 * Created by Renso on 11/09/20.
 */
interface ICallback {
    interface IGroup{
        fun editGroup(group: Group?)
        fun deleteGroup(group: Group?)
    }

    interface ITask{
        fun editTask(task: Task?)
        fun deleteTask(task: Task?)
        fun updateTask(task: Task?)
    }
}