package com.contreras.comsatel

import android.app.Application
import android.content.Context
import com.contreras.comsatel.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Renso on 30/08/20.
 */
class ComsatelApplication: Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: ComsatelApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@ComsatelApplication)
            modules(appModule)
        }
    }
}