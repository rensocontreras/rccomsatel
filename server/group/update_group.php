<?php
ini_set('display_errors',1);
include('./../class/classMySQL.php');

$inputJSON = file_get_contents('php://input');
$isCorrected = false;

if(isset($inputJSON)){
    $content = json_decode( $inputJSON, TRUE );
    $base     = new cMySQL();

    $group_id = $content['txt_group_id'];
    $name     = $content['txt_name'];
    $status = 0;
    $sql      = "update table_group set name= '$name' where id='$group_id'";
    $response = $base->ejecutar($sql);
    if($response) $isCorrected = true;

    $base->desconectar();
}

if($isCorrected){
    $status  = 200; 
    $resjson = "Se ha creado el grupo correctamente";
}
else{
    $status  = 400; 
    $resjson = "No se ha podido crear el grupo, intentelo otra vez";
}

$arr = array('status' => $status, 'message' => $resjson);
echo json_encode($arr);
?>