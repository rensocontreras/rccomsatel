<?php
ini_set('display_errors', 1);
include('./../class/classMySQL.php');

$inputJSON = file_get_contents('php://input');

$isCorrected = false;
if (isset($inputJSON)) {
    $content = json_decode($inputJSON, TRUE);
    $base = new cMySQL();

    $status = $content['txt_status'];
    $sql = "Select * from table_group where status='$status'";
    $result = $base->consultar($sql, "assoc");
    $isCorrected = true;
    $base->desconectar();
}

if ($isCorrected){
    $status  = 200; 
    $resjson = "Se han obtenido los grupos correctamente";
}else{
    $status  = 400; 
    $resjson = "No se han podido obtener los grupos, intentelo otra vez";
} 

$arr = array('status' => $status, 'message' => $resjson, 'data'=>$result);
echo json_encode($arr);
?>