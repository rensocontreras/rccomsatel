<?php
ini_set('display_errors', 1);
include('./../class/classMySQL.php');

$inputJSON = file_get_contents('php://input');

$isCorrected = false;
if (isset($inputJSON)) {
    $content = json_decode($inputJSON, TRUE);
    $base = new cMySQL();

    $id       = $content['txt_group_id'];
    $sql      = "Delete from table_group where id='$id'";
    $response = $base->ejecutar($sql);
    if ($response) {
        $sql      = "Delete from table_task where group_id='$id'";
        $response = $base->ejecutar($sql);
        if ($response) $isCorrected = true;
    }

    $base->desconectar();
}

if ($isCorrected){
    $status  = 200; 
    $resjson = "Se ha eliminado el grupo correctamente";
} else{
    $status  = 400; 
    $resjson = "No se ha podido eliminar el grupo, intentelo otra vez";
} 

$arr = array('status' => $status, 'message' => $resjson);
echo json_encode($arr);
?>