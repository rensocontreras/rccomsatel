<?php
ini_set('display_errors', 1);
include('./../class/classMySQL.php');

$inputJSON = file_get_contents('php://input');

$isCorrected = false;
if (isset($inputJSON)) {
    $content = json_decode($inputJSON, TRUE);
    $base = new cMySQL();

    $group_id = $content['txt_group_id'];
    $sql = "Select * from table_task where group_id='$group_id'";
    $result = $base->consultar($sql, "assoc");
    $isCorrected = true;
    $base->desconectar();
}

if ($isCorrected){
    $status  = 200; 
    $resjson = "Se ha obtenido las tareas correctamente";
}else{
    $status  = 400; 
    $resjson = "No se han podido obtener las tareas, intentelo otra vez";
} 

$arr = array('status' => $status, 'message' => $resjson, 'data'=>$result);
echo json_encode($arr);
?>
