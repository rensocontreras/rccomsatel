<?php
ini_set('display_errors', 1);
include('./../class/classMySQL.php');

$inputJSON = file_get_contents('php://input');

$isCorrected = false;
if (isset($inputJSON)) {
    $content = json_decode($inputJSON, TRUE);
    $base = new cMySQL();

    $id       = $content['txt_task_id'];
    $name   = $content['txt_name'];
    $sql      = "update table_task set name='$name' where id='$id'";
    $response = $base->ejecutar($sql);
    if ($response) $isCorrected = true;
    $base->desconectar();
}

if ($isCorrected){
    $status  = 200; 
    $resjson = "Se ha actualizado la tarea correctamente";
}else{
    $status  = 400; 
    $resjson = "No se ha podido actualizado la tarea, intentelo otra vez";
} 

$arr = array('status' => $status, 'message' => $resjson);
echo json_encode($arr);
?>
