<?php
ini_set('display_errors', 1);
include('./../class/classMySQL.php');

$inputJSON = file_get_contents('php://input');

$isCorrected = false;
if (isset($inputJSON)) {
    $content = json_decode($inputJSON, TRUE);
    $base = new cMySQL();

    $id = $content['txt_task_id'];
    $sql = "Select * from table_task where id='$id'";
    $result = $base->consultar($sql, "assoc");
    if (!empty($result)) {
        $group_id = $result[0]["group_id"];  //o      $result["group_id"]
        $sql      = "Delete from table_task where id='$id'";
        $response = $base->ejecutar($sql);
        if ($response) {
            $sql    = "Select * from table_task where group_id='$group_id' and status=0";
            $result = $base->consultar($sql, "assoc");
            if (empty($result)) {  //No hay actividades en el grupo con status 0
                $sql   = "Select * from table_task where group_id='$group_id'";
                $result = $base->consultar($sql, "assoc");
                if (empty($result)){  //No hay actividades en el grupo
                    $sql      = "update table_group set status= 0, total = 0 where id='$group_id'";
                    $response = $base->ejecutar($sql);
                    if ($response) $isCorrected = true;
                }else{
                    $sql      = "update table_group set status= 1, total = total -1 where id='$group_id'";
                    $response = $base->ejecutar($sql);
                    if ($response) $isCorrected = true;
                }
            } else {
                $sql      = "update table_group set status= 0, total = total -1 where id='$group_id'";
                $response = $base->ejecutar($sql);
                if ($response) $isCorrected = true;
            }
        }
    }

    $base->desconectar();
}

if ($isCorrected){
    $status  = 200; 
    $resjson = "Se ha eliminado la tarea correctamente";
}else{
    $status  = 400; 
    $resjson = "No se ha podido eliminar la tarea, intentelo otra vez";
} 

$arr = array('status' => $status, 'message' => $resjson);
echo json_encode($arr);
?>
