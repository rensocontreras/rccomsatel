<?php
ini_set('display_errors', 1);
include('./../class/classMySQL.php');

$inputJSON = file_get_contents('php://input');

$isCorrected = false;
if (isset($inputJSON)) {
    $content = json_decode($inputJSON, TRUE);
    $base = new cMySQL();

    $id       = $content['txt_task_id'];
    $status   = $content['txt_status'];
    $sql      = "update table_task set status='$status' where id='$id'";
    $response = $base->ejecutar($sql);

    if ($response) {
        $sql   = "Select * from table_task where id='$id'";
        $result = $base->consultar($sql, "assoc");
        if (!empty($result)) {
            $group_id = $result[0]["group_id"];  //o      $result["group_id"]
    
            $sql   = "Select count(*) as mtotal from table_task where group_id='$group_id'";
            $result = $base->consultar($sql, "assoc");
            $currentTotal = $result[0]["mtotal"];
            if($currentTotal==0){
                $sql = "update table_group set status= 0 where id='$group_id'";
                $response = $base->ejecutar($sql);
                if ($response) $isCorrected = true;
            } else{
                $sql   = "Select count(*) as mtotal from table_task where group_id='$group_id' and status= 0";
                $result = $base->consultar($sql, "assoc");
                $currentTotal = $result[0]["mtotal"];
                if($currentTotal==0) $sql = "update table_group set status= 1 where id='$group_id'";
                else $sql = "update table_group set status= 0 where id='$group_id'";
    
                $response = $base->ejecutar($sql);
                if ($response) $isCorrected = true;
            }
        }
    }
    $base->desconectar();
}

if ($isCorrected){
    $status  = 200; 
    $resjson = "Se ha actualizado la tarea correctamente";
}else{
    $status  = 400; 
    $resjson = "No se ha podido actualizado la tarea, intentelo otra vez";
} 
//    $resjson = "Se ha actualizado la tarea correctamente ".$result[0]["mtotal"];


$arr = array('status' => $status, 'message' => $resjson);
echo json_encode($arr);
?>
