-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 12, 2020 at 06:30 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u509367887_bdcomsatel`
--

-- --------------------------------------------------------

--
-- Table structure for table `table_group`
--

CREATE TABLE `table_group` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `table_group`
--

INSERT INTO `table_group` (`id`, `name`, `status`, `total`) VALUES
(56, 'Labores del día', 0, 2),
(57, 'Medicinas', 0, 0),
(58, 'Proyecto comsatel', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `table_task`
--

CREATE TABLE `table_task` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `table_task`
--

INSERT INTO `table_task` (`id`, `group_id`, `name`, `status`) VALUES
(65, 56, 'Reunión diaria', 1),
(66, 56, 'Coordinar Reunión', 0),
(67, 58, 'Crear repo', 1),
(68, 58, 'Crear servicios en php', 1),
(69, 58, 'Maquetar', 1),
(70, 58, 'Consumir servicios', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table_group`
--
ALTER TABLE `table_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `table_task`
--
ALTER TABLE `table_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_group` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table_group`
--
ALTER TABLE `table_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `table_task`
--
ALTER TABLE `table_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `table_task`
--
ALTER TABLE `table_task`
  ADD CONSTRAINT `fk_group` FOREIGN KEY (`group_id`) REFERENCES `table_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
