<?php
include('config.php');
class cMySQL{
    private $cnx;
    private $h;
    private $u;
    private $p;
    private $b;
	private $r;
    #==========================
    function __construct(){
        $this->h=HOST;
        $this->u=USER;
        $this->p=PASS;
        $this->b=BASE;
		$this->r=PORT;
    }
    #==========================
    private function conectar(){
        $this->cnx=new mysqli($this->h,$this->u,$this->p,$this->b,$this->r) or die('Error al conectar a BASE');
        $this->cnx->set_charset('utf8');
    }
    #==========================
    function desconectar(){
        mysqli_close($this->cnx);
    }
    #==========================
    function consultar($sql="",$formato=""){
        $this->conectar();
        $resultados=$this->cnx->query($sql); //El objeto cnx hace referencia al metodo query a la cual le paso $sql
        
        $salida = array();
            switch($formato){
                case 'json': //TODAS FILAS EN JSON
                    while($fila=$resultados->fetch_assoc()){
                        $salida[]=$fila;
                    }
                    return json_encode($salida);
                    break;
                    
                case 'assoc':
                    while($fila=$resultados->fetch_assoc()){
                        $salida[]=$fila;
                    }
                    return $salida;
                    break;    
            }
        
    }
    #==========================
    function ejecutar($sql){
        $this->conectar();
        $exito=$this->cnx->query($sql) or die("Error en consulta:<br/>$sql");
        #devuelve 1 cuando se realizó correctamente
        return $exito;
    }
}

?>