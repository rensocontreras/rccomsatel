# rccomsatel

**PROYECTO COMSATEL**

1. Los servicios fueron creados con **PHP** usando **mysql**

2. Para el desarrollo del front, se ha realizado con **kotlin**, usando **MVVM**, **retrofit** y **Koin**

3. Como se está usando el viewModel, en algunos casos ya no es necesario restaurar las pantallas, la única pantalla que tiene restauración es taskActivity.

4. Para el navigationBottom se ha utilizado **navigation** de **JetPack**

5. La parte back y front serán compartidas, gracias.

6. En otra rama se ha aplicado el **Databinding** usando adapters customizados, recyclerViews y viewModel
